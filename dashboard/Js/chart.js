$( document ).ready(function() {
   var isPermission = $('#isPermission').val();
   if(isPermission=='true'){
        getTotalOrderSales();
        getCustomerWiseSale();
   }
});

function getTotalOrderSales(){
    
   $('#todayOrder').text(0);
   $('#todaySales').text(0.0);
   $('#monthlySale').text(0.0);
   
   var  jsonData = {};
   var result = postAjaxCall("report.do?frm=getTotalOrderSales",jsonData);
//   console.log(result)
   
   if(result.respCode==200){
       var data = result.data;
       $('#todayOrder').text(data.todayOrder);
       $('#todaySales').text(data.todaySales);
       $('#monthlySale').text(data.monthlySales);
   }
}

function getCustomerWiseSale(){
    
    var  jsonData = {};
    var result = postAjaxCall("report.do?frm=getCustomerWiseSale",jsonData);
    var seriesData = result.data;
   
    // Create the chart
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Report'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total Sale'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: 'Rs. {point.y:.1f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <br>Total sale Rs. <b>{point.y:.2f}</b><br/>'
        },
        series: [
            {
                name: "Name",
                colorByPoint: true,
                data: seriesData
            }
        ]
    });
}


function postAjaxCall(url,data) {
    var result;
    $.ajax({
        type: "POST",
        url: url,
        async: false,
        dataType: 'json',
        data:data,
        success: function(data) {
            result = data;
        }
    });
    return result;
}