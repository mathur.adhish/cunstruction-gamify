$( document ).ready(function() {
     $('#orderTypeDesc').multiselect();
     $('#orderTypeDesc').change(function () {
         $("#orderTypeDescInp").val($(this).val());
     });
     
     $('#variationDesc').multiselect();
     $('#variationDesc').change(function () {
         $("#variationDescInp").val($(this).val());
     });
     
     
     $("#updateCategory").validate({
        rules: {
            "dishName": {
                required: true,
            },
            "price": {
                required: true,
            }
        },
        messages: {
            "dishName": {
                required: "Dish name required"
            },
            "price": {
                required: "Price required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function updateForm(){
    if($('#updateCategory').valid()){
         document.updateCategory.submit();
    }
}
 
   