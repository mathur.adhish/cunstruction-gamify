$( document ).ready(function() {
     $('#orderTypeDesc').multiselect();
     $('#orderTypeDesc').change(function () {
         $("#orderTypeDescInp").val($(this).val());
     });
     
     $('#variationDesc').multiselect();
     $('#variationDesc').change(function () {
         $("#variationDescInp").val($(this).val());
     });
     
    
     $("#createCategory").validate({
        rules: {
            "dishName": {
                required: true,
            },
            "sortName": {
                required: true,
            },
            "price": {
                required: true,
            }
        },
        messages: {
            "dishName": {
                required: "Dish name required"
            },
            "sortName": {
                required: "Sort name required"
            },
            "price": {
                required: "Price name required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}

function onSelectOrderTyep(){
    
}
