$( document ).ready(function() {
     $("#createCategory").validate({
        rules: {
            "desc": {
                required: true,
            }
        },
        messages: {
            "desc": {
                required: "Status name required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}
