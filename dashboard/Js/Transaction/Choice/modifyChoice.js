$( document ).ready(function() {
     $("#updateCategory").validate({
        rules: {
            "orderTypeDesc": {
                required: true,
            }
        },
        messages: {
            "orderTypeDesc": {
                required: "Order Type required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function updateForm(){
    if($('#updateCategory').valid()){
         document.updateCategory.submit();
    }
}
 
   