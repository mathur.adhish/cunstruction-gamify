$( document ).ready(function() {
     $("#createCategory").validate({
        rules: {
            "locationName": {
                required: true,
            }
        },
        messages: {
            "locationName": {
                required: "Location name required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}
