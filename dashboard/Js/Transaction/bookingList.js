$( document ).ready(function() {
    $( ".datepicker-13" ).datepicker({ dateFormat: 'yy-mm-dd' });
    showReport(0);
    
    $(document).on('click','.editRecord',function() {
        editBooking(this);
    });
    
    $(document).on('click','.viewRecord',function() {
        viewBooking(this);
    });
    
});

function search(){
   $('#currentPage').val(1);
   showReport(0);
}

function showReport(offset){

    $("#recordTable").empty().html('<img src="../images/loading.gif"/>');
    $('#pagination-bar').empty();
 
    var data = "fromDate="+$('#fromDate').val()+
               "&toDate="+$('#toDate').val()+
               "&orderId="+$('#orderId').val()+
               "&customerName="+$('#customerName').val()+
               "&locationDesc="+$('#locationDesc').val()+
               "&customerMob="+$('#customerMob').val()
               +"&limit="+$('#limit').val()
               +"&offset="+offset;
    $.ajax({
        type: "POST",
        timeout: 120000,
        url:  "../TransactionAjax/bookingListAjax.jsp?",
        data:data,
        success: function(res){
            $('#recordTable').html(res);
           
            //**Pagination**/
            var limit = parseInt($('#limit').val());
            var count = parseInt($('#totalReords').val());
            var currentPage= parseInt($('#currentPage').val());
            
            var totalPagination=1;
            if(limit!==0 && limit<count){
                 totalPagination= Math.ceil(count/limit);
            }
            
            $('#pagination-bar').append('<ul id="pagination-no" class="pagination-sm"></ul>');
            $('#pagination-no').twbsPagination({
                totalPages: totalPagination,
                visiblePages: 5,
                hideOnlyOnePage:false,
                initiateStartPageClick:false,
                startPage: currentPage,

                next: 'Next',
                prev: 'Prev',
                onPageClick: function (event, page) {
                    $('#currentPage').val(page);
                    offset = (limit*(page-1));
                    showReport(offset);
                }
            });
        },
        error : function (err){
            console.log(err);
            $("#recordTable").html("Internal server error"+err.responseText);
        }
    });
}

function editBooking(elm){
    var w =800; var h=500;
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var id= jQuery.trim($(elm).attr('id').substring(8));
    var targetWin = window.open ("updateBooking.jsp?orderTransId="+id, "Update booking", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function viewBooking(elm){
    var w =800; var h=500;
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var id= jQuery.trim($(elm).attr('id').substring(8));
    var targetWin = window.open ("viewBookingIteamList.jsp?orderTransId="+id, "Update booking", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

   
 function updateSuccess(message){
     $("#errorDiv").text(message);
     var limit = parseInt($('#limit').val());
     var currentPage= parseInt($('#currentPage').val());
     var  offset = (limit*(currentPage-1));
     showReport(offset);
 }  
 
function updateBookingStatusAjax(elm){
    var status = $(elm).val();
    var id= $.trim($(elm).attr('id').substring(6));
    
    $.ajax({
        type: "POST",
        timeout: 120000,
        url:  "../booking.do?frm=updateBookingAjax",
        data:{
            "bookingId":id,
            "status":status
        },
        success: function(res){
          $("#errorDiv").text(res.respMsg);
        },
        error : function (err){
            console.log(err);
            $("#recordTable").html("Internal server error"+err.responseText);
        }  
    });
}
