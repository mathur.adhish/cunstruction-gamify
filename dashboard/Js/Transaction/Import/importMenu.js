$( document ).ready(function() {
     $("#importForm").validate({
        rules: {
            "file": {
                required: true,
            }
        },
        messages: {
            "file": {
                required: "File required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#importForm').valid()){
         document.importForm.submit();
    }
}
