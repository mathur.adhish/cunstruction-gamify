$( document ).ready(function() {
     $("#createCategory").validate({
        rules: {
            "variationDesc": {
                required: true,
            }
        },
        messages: {
            "variationDesc": {
                required: "Variation desc required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}
