$( document ).ready(function() {
    jQuery.validator.addMethod("userRoleRequired", function(value, element) {
        var isExist = true;
        if(value==0){isExist=false;}
        return isExist;
    }, "User role required ");


     $("#updateForm").validate({
        rules: {
            "userName": {
                required: true,
            },
            "password": {
                required: true,
            },
            "name": {
                required: true,
            },
            "userRole": {
                userRoleRequired : true 
            }
        },
        messages: {
            "userName": {
                required: "User name required"
            },
            "password": {
                required: "Password required",
            },
            "name": {
                required: "Name required",
            },
            "userRole": {
                required: "User role required",
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
    
    $(document).on('click','.allcheck',function() {
        var className = $(this).attr('data-class');
        if ($(this).is(':checked')) {
           $("."+className).prop('checked', true);
        }else {
          $("."+className).prop('checked', false);
        }
    }); 
    
});
  
function updateForms(){
    if($('#updateForm').valid()){
         document.updateForm.submit();
    }
}
 
function onCheckSubMenu(elm){
    var className = $(elm).attr('class');
    var checked = false;
    $('.'+className).each(function() {
        if($(this).is(":checked")){
             checked=true;
        }
    });
    if(checked){
      $("[data-class='"+className+"']").prop('checked', true);
    }else{
      $("[data-class='"+className+"']").prop('checked', false);
    }
}
   