$( document ).ready(function() {
     $("#createCategory").validate({
        rules: {
            "orderTypeDesc": {
                required: true,
            }
        },
        messages: {
            "orderTypeDesc": {
                required: "Order Type required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}
