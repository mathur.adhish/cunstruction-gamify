$( document ).ready(function() {
     $("#updateCategory").validate({
        rules: {
            "taxName": {
                required: true,
            },
            "texPercentage": {
                required: true,
            }
        },
        messages: {
            "taxName": {
                required: "Tax name required"
            },
            "texPercentage": {
                required: "Tax percentage required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function updateForm(){
    if($('#updateCategory').valid()){
         document.updateCategory.submit();
    }
}
 
   