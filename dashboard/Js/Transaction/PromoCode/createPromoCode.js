$( document ).ready(function() {
    
    $('#remarks').tagsInput({
         'width':'100%',
    });
    
    $('#validFrom').datetimepicker({
            format: 'YYYY-MM-DD  HH:mm:ss' 
    });
    
    $('#validTo').datetimepicker({
            format: 'YYYY-MM-DD  HH:mm:ss'
    });
                
    jQuery.validator.addMethod("greaterThanZero", function(value, element) {
        var isExist = true;
        $.ajax({
            url : '../master.do?frm=checkPpromoCode',
            type : 'GET',
            async: false,
            data : {
                'promoCodeName' : value
            },
            dataType:'json',
            success : function(data) {              
               isExist=false;
               if(data.respCode==200){
                   isExist=true;
               }
            }
        });
        return isExist;
    }, "Promo code alredy exists ");

     $("#createCategory").validate({
       rules: {
            "promCodeName": {
                required: true,
                greaterThanZero : true 
            },
            "promCodeType": {
                required: true,
            },
            "amount": {
                required: true,
            }
        },
        messages: {
            "promCodeName": {
                required: "Prom code name required"
            },
            "promCodeType": {
                required: "Prom code type required",
            },
            "amount": {
                required: "Amount required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});



function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}
