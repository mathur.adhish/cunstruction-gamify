$( document ).ready(function() {
    $('#remarks').tagsInput({
         'width':'100%',
    });
    
    $('#validFrom').datetimepicker({
            format: 'YYYY-MM-DD  HH:mm:ss' 
    });
    
    $('#validTo').datetimepicker({
            format: 'YYYY-MM-DD  HH:mm:ss'
    });
    
     
     $("#updateCategory").validate({
        rules: {
            "promCodeName": {
                required: true,
            },
            "promCodeType": {
                required: true,
            },
            "amount": {
                required: true,
            }
        },
        messages: {
            "promCodeName": {
                required: "Prom Code name required"
            },
            "promCodeType": {
                required: "Prom Code type required",
            },
            "amount": {
                required: "Amount required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function updateForm(){
    if($('#updateCategory').valid()){
         document.updateCategory.submit();
    }
}
 
   