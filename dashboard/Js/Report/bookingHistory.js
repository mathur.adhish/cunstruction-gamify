$(function() { 
    $( ".datepicker-13" ).datepicker({ dateFormat: 'yy-mm-dd' });
    showReport(0);
});

function search(){
   $('#currentPage').val(1);
   showReport(0);
}

function searchAll(){
    $('#fromDate').val('');
    $('#toDate').val('');
    $('#orderId').val('');
    $('#customerName').val('');
    $('#customerMob').val('');
    $('#currentPage').val(1);
    showReport(0);
}

function showReport(offset){

    $("#recordTable").empty().html('<img src="../images/loading.gif"/>');
    $('#pagination-bar').empty();
     
    var data = "fromDate="+$('#fromDate').val()+
               "&toDate="+$('#toDate').val()+
               "&orderId="+$('#orderId').val()+
               "&customerName="+$('#customerName').val()+
               "&locationDesc=0"+
               "&customerMob="+$('#customerMob').val()
               +"&limit="+$('#limit').val()
               +"&offset="+offset;
    $.ajax({
        type: "POST",
        timeout: 120000,
        url:  "../ReportAjax/bookingHistoryAjax.jsp?",
        data:data,
        success: function(res){
            $('#recordTable').html(res);
            
            //**Pagination**/
            var limit = parseInt($('#limit').val());
            var count = parseInt($('#totalReords').val());
            var currentPage= parseInt($('#currentPage').val());
            
            var totalPagination=1;
            if(limit!==0 && limit<count){
                 totalPagination= Math.ceil(count/limit);
            }
            
            $('#pagination-bar').append('<ul id="pagination-no" class="pagination-sm"></ul>');
            $('#pagination-no').twbsPagination({
                totalPages: totalPagination,
                visiblePages: 5,
                hideOnlyOnePage:false,
                initiateStartPageClick:false,
                startPage: currentPage,

                next: 'Next',
                prev: 'Prev',
                onPageClick: function (event, page) {
                    $('#currentPage').val(page);
                    offset = (limit*(page-1));
                    showReport(offset);
                }
            });
        },
        error : function (err){
            console.log(err);
            $("#recordTable").html("Internal server error"+err.responseText);
        }
    });
}